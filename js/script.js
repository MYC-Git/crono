  $(function() {
  	localStorage.c = (localStorage.c || "0.0");
  	localStorage.l = (localStorage.l || "");
  	var t, cl = $("#crono");
  	var listar = $("#lista");
  	listar.html(localStorage.l);

  	function incr() {
  		localStorage.c = +localStorage.c + 0.1;
  	}

  	function mostrar() {
  		cl.html((+localStorage.c).toFixed(1));
  	};

  	function arrancar() {
  		t = setInterval(function() {
  			incr();
  			mostrar()
  		}, 100);
  	};

  	function parar() {
  		clearInterval(t);
  		t = undefined;
  		localStorage.l += ('<p>' + "Parado en " + cl.html() +" segundos." + '</p>');
  	};

  	function cambiar() {
  		if (!t) arrancar();
  		else {
  			listar.append('<p>' + "Parado en " + cl.html() + " segundos." + '</p>');
  			parar();
  		}
  	};
  	$("#cambiar").on('click', cambiar);
  	$("#toque").on('tap', cambiar);
  	$("#inicializar").on('click', function() {
  		if (!t) {
  			localStorage.c = "0.0";
  			localStorage.l = "";
  			$("#lista").empty();
  			localStorage.l = "";
  			mostrar();
  		}
  	});
  	$("#toque").on('swipe', function() {
  		if (!t) {
  			localStorage.c = "0.0";
  			localStorage.l = "";
  			$("#lista").empty();
  			localStorage.l = "";
  			mostrar();
  		}
  	});
  	mostrar();
  });
