# Cronómetro
WebApp realizada como ejercicio obligatorio del módulo 8 de la 4ª edición del curso de la universidad politécnica de Madrid en Miriada x: Desarrollo en HTML5, CSS y Javascript de WebApps, incl. móviles FirefoxOS.  
Obtenga más [información](https://myc-git.gitlab.io/mycweb/paginas/javascript/crono.html) o vealo en [funcionamiento](https://myc-git.gitlab.io/crono/).